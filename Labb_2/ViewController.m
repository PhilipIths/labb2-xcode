//
//  ViewController.m
//  Labb_2
//
//  Created by Stjernström on 2015-01-26.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import "ViewController.h"
#import "Story.h"

@interface ViewController()

@property (weak, nonatomic) IBOutlet UITextView *storyView;

@end

@implementation ViewController

- (IBAction)generate:(id)sender {
    Story *gen = [[Story alloc] init];
    self.storyView.text = [gen storyGenerator];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
