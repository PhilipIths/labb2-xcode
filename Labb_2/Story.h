//
//  Story.h
//  Labb_2
//
//  Created by Stjernström on 2015-01-26.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Story : NSObject

- (NSString*)storyGenerator;

@end
