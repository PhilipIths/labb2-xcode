//
//  Story.m
//  Labb_2
//
//  Created by Stjernström on 2015-01-26.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import "Story.h"

@interface Story()

@property (nonatomic) NSArray *characters;
@property (nonatomic) NSArray *act1;
@property (nonatomic) NSArray *situation1;
@property (nonatomic) NSArray *situation2;
@property (nonatomic) NSArray *act2;
@property (nonatomic) NSArray *trouble;
@property (nonatomic) NSArray *act3;
@property (nonatomic) NSArray *end;

@end

@implementation Story

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.characters = @[@"Man", @"Cat", @"Woman", @"Crow", @"Turtle", @"Bear", @"Bum", @"Snail", @"Fox", @"Pig", @"Teenager", @"Skunk", @"Parrot"];
        self.act1 = @[@"sleeping on a dumpster", @"eating pastries", @"running a marathon", @"selling rats"];
        self.situation1 = @[@"angry chefs were running around", @"someone was typing very loudly on their laptop", @"a group of kids were going after an ice-cream truck", @"frogs were raining down from the sky"];
        self.situation2 = @[@"soothing silence", @"blizzard", @"particularly mellow moment", @"loud stampede of pigeons"];
        self.act2 = @[@"tried to wear clothes that were too small", @"made lasagna out of pants", @"finished writing a novel about a chair", @"ran into a wall of hippos"];
        self.trouble = @[@"somehow ended up causing world war three to happen", @"made a volcano appear", @"accidentally created an Elephant that was too big", @"accidentally summoned satan"];
        self.act3 = @[@"eating all the cheese", @"doing a really stupid-looking dance", @"punching a clown", @"racing a cow"];
        self.end = @[@"just sort of resolved itself", @"was made of pudding", @"looked like the surface of the moon", @"was fine", @"smelled like garbage"];
    }
    return self;
}

-(NSString*)randomElement:(NSArray*)array {
    return array[arc4random() % array.count];
}

- (NSString*)storyGenerator {
    
    NSString *mainChar = [self randomElement:self.characters];
    NSString *firstAct = [self randomElement:self.act1];
    NSString *firstSituation = [self randomElement:self.situation1];
    NSString *secondSituation = [self randomElement:self.situation2];
    NSString *secondAct = [self randomElement:self.act2];
    NSString *conflict = [self randomElement:self.trouble];
    NSString *thirdAct = [self randomElement:self.act3];
    NSString *theEnd = [self randomElement:self.end];
    NSString *completeStory = [NSString stringWithFormat:@"Once upon a time, there was a %@. This %@ was %@ while %@. But later, during a %@, the %@ %@, and %@. Good one, %@! However, the %@ attempted to fix this by %@, and then everything %@. The end.", mainChar, mainChar, firstAct, firstSituation, secondSituation, mainChar, secondAct, conflict, mainChar, mainChar, thirdAct, theEnd];
    return completeStory;
}

@end
